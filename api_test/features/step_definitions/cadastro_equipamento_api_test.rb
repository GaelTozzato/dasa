  Dado("tenho acesso a API") do
  end
  
  Quando("eu requisitar um post na API") do
    @body = {
        "nome": "GABRIELTOSATO",
        "ip": "192.168.1.2",
        "porta": 8080,
        "protocolo": "ASTM",
        "bidirecional": true,
        "fazQuery": true,
        "ativo": true,
        "conectado": false,
        "forcarConexaoDigiboard": true
        }.to_json
        @url_base = HTTParty.post 'http://contigencia-hospitalar-dev.azurewebsites.net/equipamentos',
        :body => @body,
        :headers => {"Content-Type" => 'application/json'}
  end
  
  Então("valido os dados foram inseridos") do
    puts @url_base.body
    puts @url_base.message
    expect(@url_base.code).to eq 200
  end
  