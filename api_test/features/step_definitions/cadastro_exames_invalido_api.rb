Dado("que tenho acesso a API de exames") do
  end
  
  Quando("eu requisitar um post invalido para API") do
    @body = {
        "codigo": "TSHA",
        "codigoInterface": "COD A",
        "material": "SORO",
        "equipamento": "GABRIELTOSATO"
        }.to_json
        @url_base = HTTParty.post 'http://contigencia-hospitalar-dev.azurewebsites.net/exames',
        :body => @body,
        :headers => {"Content-Type" => 'application/json'}
  end
  
  Então("valido os dados não foram inseridos corretamente") do
    puts @url_base.body
    puts @url_base.message
    expect(@url_base.code).to eq 409
  end
  