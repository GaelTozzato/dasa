Dado("que tenha acesso a API") do
  end
  
  Quando("eu fizer um post para API") do
    @body = {
        "nome": "Gabriel Tosato Pires",
        "nascimento": "09/06/1996",
        "sexo": "M",
        "leito": "Leito A",
        "centroDeOrigem": "Centro A",
        "exames": [
        {"codigo": "GLICOA"}, {"codigo": "TSHA"}
        ]
        }.to_json
        @url_base = HTTParty.post 'http://contigencia-hospitalar-dev.azurewebsites.net/ordens',
        :body => @body,
        :headers => {"Content-Type" => 'application/json'}
  end
  
  Então("validar dados foram inseridos corretamente") do
    puts @url_base.body
    puts @url_base.message
    expect(@url_base.code).to eq 200
  end
  