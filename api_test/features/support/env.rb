require 'rspec'
require 'cucumber'
require 'selenium-webdriver'
require 'capybara'
require 'capybara/cucumber'
require 'page-object'
require 'data_magic'
require 'pry'
require 'httparty'

World(PageObject::PageFactory)