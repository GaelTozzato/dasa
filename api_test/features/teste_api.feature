#language:pt
#utf-8

Funcionalidade: Validação de API
Eu como sistema intermediario
Quero fazer uma requisição dos dados
Para validar as informações da API

	@cadastro_equipamento_api
	Cenario: Cadastro de equipamento via API
		Dado tenho acesso a API
		Quando eu requisitar um post na API
		Então valido os dados foram inseridos

    @cadastro_exames_api
    Cenario: Cadastro de exames via API
		Dado que tenho acesso a API
		Quando eu requisitar um post para API
		Então valido os dados foram inseridos corretamente
    
    @cadastro_ordens_api
    Cenario: Cadastro de ordens via API
		Dado que tenha acesso a API
		Quando eu fizer um post para API
		Então validar dados foram inseridos corretamente

    
    @validar_cadastro_ordens_api
    Cenario: Validar cadastro de ordens via API
		Dado que tenha acesso a uma API
		Quando eu realizar um get para API
		Então validar dados corretamente

	@cadastro_exames_invalido_api
    Cenario: Cadastro de exames invalidos via API
		Dado que tenho acesso a API de exames
		Quando eu requisitar um post invalido para API
		Então valido os dados não foram inseridos corretamente
    